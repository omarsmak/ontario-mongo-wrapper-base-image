#Ontario Base Image Bundled With MongoDB 

This Image is a backbone for any Ontario wrapper with these extensions `CSV and JSON`.

This image has be in included in any dataset images. Please aware of these conventions:

1. The dataset file will be the same name as MongoDB collection, this is can be changed from the Dockerfile.
2. The dataset file has to reside under {/tmp/datasets}.
3. The mapping file has to reside under {/tmp/datasets/mapping.ttl}.



##Usage:
Just include this image under your image with the datasets. Example:
~~~~
FROM omarsmak/ontario-mongo-wrapper-base-image

COPY data/. /tmp

ENTRYPOINT ["/app/run.sh"]
~~~~ 

Note:
Please include `ENTRYPOINT ["/app/run.sh"]` in every dataset image you create.


Docker command:
`docker run -i -t --rm -p 27001:27001 {image_name}`


