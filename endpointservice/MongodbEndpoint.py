#!/usr/bin/env python

from flask import Flask, request
from flask.json import jsonify
import sys
import os

from mongodbwrapper.SimpleWrapper import SimpleWrapper

app = Flask(__name__)


@app.route("/sparql", methods=['POST', 'GET'])
def sparql():
    if request.method == 'GET':
        try:
            molecule = request.args.get("molecule", '')
            query = request.args.get("query", '')
            query = query.replace('\n', ' ').replace('\r', ' ')
            #Prepare env
            mongurl = "{}:{}".format(database_host,database_port)
            sw = SimpleWrapper(molecule, mapping, mongurl, collection_name, database_name)
            res = sw.exeQuery(query)
            return jsonify({"result": res})
        except Exception as e:
            print e
            return jsonify({"result": [], "error": e.message})
    else:
        return jsonify({"result": [], "error": "Invalid HTTP method used. Use GET "})


def usage():
    usage_str = ("Usage: {program} -p <port>  -m <path_to_mapping_file>  "
                 + "\n where \n<port> "
                 + " is port for this wrapper \n<path_to_mapping_file> "
                   " is path to mapping file (.ttl, .nt, ..) for this wrapper"
                 + "\n")

    print usage_str.format(program=sys.argv[0]),


if __name__ == "__main__":

    mapping = ""
    argv = sys.argv

    port = 5000
    database_port = 27017
    collection_name = None
    database_name = None
    database_host = "127.0.0.1"
    i = 0
    for a in argv:
        i += 1
        if i + 1 > len(argv):
            break
        if argv[i] == "-p":
            port = argv[i+1]
        if argv[i] == "-m":
            mapping = os.path.abspath(argv[i+1])
        if argv[i] == "--database_port":
            database_port = argv[i+1]
        if argv[i] == "-c":
            collection_name = argv[i+1]
        if argv[i] == "--database_name":
            database_name = argv[i+1]
        if argv[i] == "--database_host":
            database_host = argv[i+1]

    if collection_name == None or database_name == None:
        print "Database empty"
        usage()
        sys.exit(1)


    if not mapping or len(mapping) == 0:
        print "maping is empty"
        print port
        usage()
        sys.exit(1)
    print port, mapping
    app.run(port=port, host="0.0.0.0")
