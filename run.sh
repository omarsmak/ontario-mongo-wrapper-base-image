#!/bin/bash
# Setup mongodb user rights
set -e

dataset_folder=/tmp/datasets
mapping_file=/tmp/datasets/mapping.ttl

echo "Starting MongoDB, please hold on...."

if [ "${1:0:1}" = '-' ]; then
	set -- mongod "$@"
fi

## allow the container to be started with `--user`
if [ "$1" = 'mongod' -a "$(id -u)" = '0' ]; then
	chown -R mongodb /data/configdb /data/db
	exec gosu mongodb "$BASH_SOURCE" "$@"
fi

if [ "$1" = 'mongod' ]; then
	numa='numactl --interleave=all'
	if $numa true &> /dev/null; then
		set -- $numa "$@"
	fi
fi

exec "$@"

# Run mongodb and fork it from the terminal
mongod --fork --logpath /var/log/mongodb.log

# Now we create our database
mongo < db_setup.js

# Now we check if dataset file exists under /data/dataset
if [ -d "$dataset_folder" ]; then
    echo "Importing data into MongoDB instance ...."
    #Check for csv dataset
    if [ -f  $dataset_folder/$DATASET_FILE_NAME.csv ]; then
        echo "Importing CSV data type from $dataset_folder"
        mongoimport -d $DATABASE_NAME --type csv --file $dataset_folder/$DATASET_FILE_NAME.csv --headerline
    fi

    #Check for json
    if [ -f $dataset_folder/$DATASET_FILE_NAME.json ]; then
        echo "Importing JSON data type from $dataset_folder"
        mongoimport -d $DATABASE_NAME --type json --file $dataset_folder/$DATASET_FILE_NAME.json
    fi
fi

#Run our application
echo "Staring Ontario MongoDB App on port $APP_PORT. Have fun!"
python endpointservice/MongodbEndpoint.py -p $APP_PORT -m $mapping_file --database_name $DATABASE_NAME -c $DATASET_FILE_NAME


